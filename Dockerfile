FROM scratch

ADD ArchLinuxARM-rpi-3-latest.tar.gz /

COPY qemu-aarch64-static /usr/bin/qemu-aarch64-static

# Install build tools
RUN pacman -Syu gcc automake autoconf libtool make pkgconfig git --noconfirm

# Install the weston build dependencies
RUN pacman -S weston wayland-protocols zlib --noconfirm

RUN git -C /usr/src clone https://github.com/wayland-project/weston.git

WORKDIR /usr/src/weston

RUN ./autogen.sh \
 && make -j9

